# uenuku
The god of rainbows, hides the pot of gold

## Principles/ Paradigms
- all known errors are handled as errors, never as exceptions
- as soon as an exception is raised it is a severe technical error, the runtime cannot be used any further and must be discarded/ restarted
- does not store persistently, this is a wide field, starts with transaction management and ends with »don't let Hibernate steal your identity«
- does not take units into consideration, neither monetary nor quantity units, just integers (up to the caller to decide scale and projection of fractions).
- specification is not dealing with local bid=0 and other's =0, thus equality, no quantity assignment is assumed
- concurrency is not an issue, it runs in a single VM in serial fashion
- tracks history of bids
- all input is validated when entering the system, all invalid input is rejected

## System Context
- running as standalone command line program
- realizes basic bidder contract
- enhances basic bidder contract with general error handling
- automatic bidding until quantity is exhausted

## Design
- simple Java program, DRY-principle, domain driven packaging, no jigsaw yet (but DDD packages take this into consideration)
- hand carved state machine to track bidding and accounting, must be replaced in case of more complex state transitions
- exchangeable strategy for bidding
- history of bids as linked blocking queue, can be drained to persistence, data warehouse, further processing...asynchronously
- jMockit in favor of Mockito, can also mock final classes, static methods and everything else
- domains registration of bidders, auctioning (one side of contract), bidding (other side of contract)

**Absolutely never**:
- remove final keyword, it is intentionally, but also don't spill it everywhere
- implement hashCode and/ or equals on entities or data transfer objects, use dedicated comparators instead
- always bring entities with IDs to life, don't let any OR-mapper steal the identity (instead add technical IDs)

# Invocation
## Build
- mvn clean install to create runnable JAR (and run tests beforehand)
- mvn pmd:pmd to create report of PMD tool (static code analysis)

## Program itself
- java -jar target/uenuku-1.0.0-SNAPSHOT.jar – provides sufficient command line parameter help

# Why is there no technical packaging?
Technical packaging or sorting by color is the case, when sources are put into packages labelled dto, service, repository or similar purely technical aspects. Imagine you would order your place by color and put all white things into the kitchen, red things into the bed room and so on. Is it really helpful to have a UserController next to a AuctionController? What if there is a package private modifier and someone simply calls UserController from AuctionController?

Instead I prefer grouping by use case. Thus bidding or auctioning are packages and bind all the classes in first place. During the project's evolution these use case packages can be further structured. Now it is absolutely viable to have technical packages like repository, service and controller or dto. And guess what, if your domains a specific enough there is no need for further structuring.

Why are there are multiple classes with same name in different packages? A bidder's perspective of a bid or the result of bidding is different than the auction's point of view. Whereas the bidder only tracks his personal record an auction knows all bidders. This requires different views onto the same idea with varying granularity.

package uenuku.auction;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import uenuku.auction.account.Account;

public class AccountMatchers {

	public static Matcher<Account> hasBalance(int balance) {
		return new ExtractingMatcher<Account, Integer>("balance", Account::getBalance, Matchers.equalTo(balance));
	}

}

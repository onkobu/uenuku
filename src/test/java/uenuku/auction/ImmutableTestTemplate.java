package uenuku.auction;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

/**
 * Assert immutability.
 *
 * @author onkobu
 *
 */
public abstract class ImmutableTestTemplate {
	private final Class<?> immutableClass;

	public ImmutableTestTemplate(Class<?> cls) {
		immutableClass = cls;
	}

	@Test
	public void isImmutable() throws Exception {
		final BeanInfo beanInfo = Introspector.getBeanInfo(immutableClass);
		final List<String> writables = new ArrayList<>();
		for (PropertyDescriptor desc : beanInfo.getPropertyDescriptors()) {
			if (desc.getWriteMethod() != null) {
				writables.add(desc.getName());
			}
		}
		Assert.assertThat(writables, Matchers.empty());
	}

	protected static Collection<Object[]> wrapList(Class<?>... classes) {
		return Arrays.stream(classes).map(cls -> new Object[] { cls }).collect(Collectors.toList());
	}
}

package uenuku.auction.bidding;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import uenuku.auction.ExtractingMatcher;

public class BidTest {
	@Test
	public void emptyBid() {
		assertThat(Bid.empty(), hasAmount(0));
	}

	public void emptyEqualsItself() {
		assertThat(Bid.empty(), equalTo(Bid.empty()));
	}

	public void sameAmountNotEquals() {
		final int amount = 61;
		assertThat(Bid.of(amount), Matchers.not(Matchers.equalTo(Bid.of(amount))));
	}

	private static Matcher<Bid> hasAmount(int amount) {
		return new ExtractingMatcher<Bid, Integer>("amount", Bid::getCashAmount, Matchers.equalTo(amount));
	}
}

package uenuku.auction.bidding;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ImmutableTest extends uenuku.auction.ImmutableTestTemplate {

	public ImmutableTest(Class<?> cls) {
		super(cls);
	}

	@Parameters(name = "{index} {0}")
	public static Collection<Object[]> parameters() {
		return wrapList(Bid.class, BiddingFigure.class, Quantity.class);
	}
}

package uenuku.auction;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class I18NResourceTest {
	private final I18NResource resource;

	public I18NResourceTest(I18NResource resource) {
		super();
		this.resource = resource;
	}

	@Parameters(name = "{index} {0}")
	public static Collection<Object[]> parameters() {
		return Arrays.stream(I18NResource.values()).map((res) -> new Object[] { res }).collect(Collectors.toList());
	}

	@Test
	public void textNotNull() {
		assertThat(resource.getText(), notNullValue());
	}
}

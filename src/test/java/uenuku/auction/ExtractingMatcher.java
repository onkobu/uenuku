package uenuku.auction;

import java.util.function.Function;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * More descriptive than {@link org.hamcrest.Matchers#hasProperty(String)} while
 * extracting a target (attribute) from a source (type).
 *
 * @author onkobu
 *
 * @param <S> Source type.
 * @param <T> Target type.
 */
public class ExtractingMatcher<S, T> extends TypeSafeDiagnosingMatcher<S> {
	private final String attributeName;
	private final Matcher<T> matcher;
	private final Function<S, T> extractor;

	public ExtractingMatcher(String attributeName, Function<S, T> extractor, Matcher<T> matcher) {
		this.attributeName = attributeName;
		this.matcher = matcher;
		this.extractor = extractor;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(" attribute ").appendText(attributeName).appendDescriptionOf(matcher);
	}

	@Override
	protected boolean matchesSafely(S item, Description mismatchDescription) {
		final T target = extractor.apply(item);
		final boolean matches = matcher.matches(target);
		if (!matches) {
			matcher.describeMismatch(target, mismatchDescription);
		}
		return matches;
	}
}

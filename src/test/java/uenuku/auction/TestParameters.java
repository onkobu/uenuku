package uenuku.auction;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TestParameters {
	public interface Builder {
		Collection<Object[]> end();
	}

	static class BuilderInvocationHandler implements InvocationHandler {
		private final List<Object[]> arguments = new ArrayList<Object[]>();

		@Override
		public Object invoke(Object arg0, Method arg1, Object[] arg2)
				throws Throwable {
			// all add-methods must be of return type Builder
			if ("add".equals(arg1.getName())) {
				arguments.add(arg2);
				return arg0;
			}

			if ("end".equals(arg1.getName())) {
				// a collection of Object[]
				return arguments;
			}
			throw new UnsupportedOperationException(
					"May call either add or end method, not " + arg1);
		}
	}

	public static <B extends Builder> B begin(Class<B> cls) {
		// To get rid of the warning, keep the cast in a single place and not
		// suppress warnings for the entire method.
		@SuppressWarnings("unchecked")
		B bldr = (B) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(),
				new Class[] { cls }, new BuilderInvocationHandler());
		return bldr;
	}
	
	public static <P> Collection<Object[]> wrap(P[] parameters) {
		return Arrays.stream(parameters).map((p)->new Object[] {p}).collect(Collectors.toList());
	}
}
package uenuku.auction.account;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class AccountTest {
	@Test
	public void withdrawZero() {
		final int amount = 12;
		final Account acc = new Account(amount);
		assertThat(acc.withdraw(0), equalTo(0));
		assertThat(acc.getBalance(), equalTo(amount));
	}

	@Test
	public void withdrawFull() {
		final int amount = 12;
		final Account acc = new Account(amount);
		assertThat(acc.withdraw(amount), equalTo(amount));
		assertThat(acc.getBalance(), equalTo(0));
	}

	@Test
	public void withdrawMore() {
		final Account acc = new Account(12);
		assertThat(acc.withdraw(12 + 1), equalTo(12));
		assertThat(acc.getBalance(), equalTo(0));
	}

	@Test
	public void withdrawMultiple() {
		final Account acc = new Account(12);
		assertThat(acc.withdraw(5), equalTo(5));
		assertThat(acc.getBalance(), equalTo(7));
		assertThat(acc.withdraw(4), equalTo(4));
		assertThat(acc.getBalance(), equalTo(3));
	}
}

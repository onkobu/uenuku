package uenuku.auction;

import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static uenuku.auction.AccountMatchers.hasBalance;

import java.util.Collection;

import org.apache.commons.cli.MissingArgumentException;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import uenuku.auction.registration.Bidder;

@RunWith(Enclosed.class)
public class AuctionRunnerTest {
	public static class ArgumentParsingTest {
		@Test
		public void noArguments() throws Exception {
			assertThat(AuctionRunner.validateInput(new String[0]), nullValue());
		}

		@Test
		public void helpShort() throws Exception {
			assertThat(AuctionRunner.validateInput(new String[] { "-h" }), nullValue());
		}

		@Test
		public void helpLong() throws Exception {
			assertThat(AuctionRunner.validateInput(new String[] { "--help" }), nullValue());
		}

		@Test(expected = MissingArgumentException.class)
		public void quantityShort() throws Exception {
			AuctionRunner.validateInput(new String[] { "-q" });
		}

		@Test(expected = MissingArgumentException.class)
		public void quantityLong() throws Exception {
			AuctionRunner.validateInput(new String[] { "--quantity" });
		}

		@Test(expected = MissingArgumentException.class)
		public void blueShort() throws Exception {
			AuctionRunner.validateInput(new String[] { "-b" });
		}

		@Test(expected = MissingArgumentException.class)
		public void blueLong() throws Exception {
			AuctionRunner.validateInput(new String[] { "--blue" });
		}

		@Test(expected = MissingArgumentException.class)
		public void redShort() throws Exception {
			AuctionRunner.validateInput(new String[] { "-r" });
		}

		@Test(expected = MissingArgumentException.class)
		public void redLong() throws Exception {
			AuctionRunner.validateInput(new String[] { "--red" });
		}
	}

	public static class InputFixtureTransferTest {
		@Test
		public void quantityShortWithValue() throws Exception {
			final int value = 70;
			assertThat(AuctionRunner.validateInput(new String[] { "-q", String.valueOf(value), "-b", "0", "-r", "0" }),
					quantityValue(value));
		}

		@Test
		public void quantityLongWithValue() throws Exception {
			final int value = 39;
			assertThat(
					AuctionRunner
							.validateInput(new String[] { "--quantity", String.valueOf(value), "-b", "0", "-r", "0" }),
					quantityValue(value));
		}

		@Test
		public void blueShortWithValue() throws Exception {
			final int value = 83;
			assertThat(AuctionRunner.validateInput(new String[] { "-b", String.valueOf(value), "-q", "0", "-r", "0" }),
					blueCashValue(value));
		}

		@Test
		public void blueLongWithValue() throws Exception {
			final int value = 13;
			assertThat(
					AuctionRunner.validateInput(new String[] { "--blue", String.valueOf(value), "-q", "0", "-r", "0" }),
					blueCashValue(value));
		}

		@Test
		public void redShortWithValue() throws Exception {
			final int value = 21;
			assertThat(AuctionRunner.validateInput(new String[] { "-r", String.valueOf(value), "-b", "0", "-q", "0" }),
					redCashValue(value));
		}

		@Test
		public void redLongWithValue() throws Exception {
			final int value = 997;
			assertThat(
					AuctionRunner.validateInput(new String[] { "--red", String.valueOf(value), "-b", "0", "-q", "0" }),
					redCashValue(value));
		}

		@Test
		public void allInvalidNumber() throws Exception {
			assertThat(AuctionRunner.validateInput(new String[] { "--red", "t", "-b", "", "-q", "?" }),
					errorList(Matchers.hasSize(3)));
		}

		@Test
		public void fullRun() throws Exception {
			Bidder.reset();
			final int value = 997;
			AuctionRunner.main(new String[] { "--red", String.valueOf(value), "-b", "0", "-q", "0" });
			assertThat(Bidder.Eyjafjallajökull.getAccount(), hasBalance(997));
			assertThat(Bidder.Kohala.getAccount(), hasBalance(0));
		}

	}

	@RunWith(Parameterized.class)
	public static class NumberValidatorTest {
		private final String input;
		private final Integer expected;

		public interface P extends TestParameters.Builder {
			P add(String input, Integer expected);
		}

		public NumberValidatorTest(String input, Integer expected) {
			super();
			this.input = input;
			this.expected = expected;
		}

		@Parameters(name = "{index} {0} {1}")
		public static Collection<Object[]> parameters() {
			return TestParameters.begin(P.class)//
					.add(null, null)//
					.add("", null)//
					.add("  ", null)//
					.add("?", null)//
					.add("ff", null)//
					.add("0xff", null)//
					.add("-12", -12)//
					.add("0", 0)//
					.add("0.0", null)//
					.add("+12", 12)//
					.add("212", 212)//
					.add("212.13", null)//
					.end();
		}

		@Test
		public void transform() {
			if (expected == null) {
				assertThat(AuctionRunner.toIntegerValue(input), Matchers.nullValue());
			} else {
				assertThat(AuctionRunner.toIntegerValue(input), Matchers.equalTo(expected));
			}
		}
	}

	static Matcher<InputFixture> quantityValue(int value) {
		return new ExtractingMatcher<InputFixture, Integer>("quantity", InputFixture::getQuantity,
				Matchers.equalTo(value));
	}

	static Matcher<InputFixture> blueCashValue(int value) {
		return new ExtractingMatcher<InputFixture, Integer>("blue cash", InputFixture::getBlueCash,
				Matchers.equalTo(value));
	}

	static Matcher<InputFixture> redCashValue(int value) {
		return new ExtractingMatcher<InputFixture, Integer>("red cash", InputFixture::getRedCash,
				Matchers.equalTo(value));
	}

	static Matcher<InputFixture> errorList(Matcher<Collection<? extends String>> mat) {
		return new ExtractingMatcher<InputFixture, Collection<? extends String>>("errors", InputFixture::getErrors,
				mat);

	}
}

package uenuku.auction.auctioning;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collections;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import uenuku.auction.ExtractingMatcher;
import uenuku.auction.Failure;
import uenuku.auction.IdentifiableBidder;
import uenuku.auction.account.Account;

public class PairBiddingResultFactoryTest {
	private static final int HIGHEST_BID = 1;
	private final Bid higherBid = Bid.ofBidder(new StaticBidder(HIGHEST_BID));
	private final IdentifiableBidder higherBidder = higherBid.getBidder();
	private final Bid lowerBid = Bid.ofBidder(new StaticBidder(HIGHEST_BID - 1));
	private final IdentifiableBidder lowerBidder = lowerBid.getBidder();
	private final Bid equalBid = Bid.ofBidder(new StaticBidder(HIGHEST_BID));
	private final IdentifiableBidder equalBidder = equalBid.getBidder();

	private final Bid zeroBidA = Bid.ofBidder(new StaticBidder(0));
	private final Bid zeroBidB = Bid.ofBidder(new StaticBidder(0));

	@Test(expected = IllegalArgumentException.class)
	public void emptyBids() {
		PairBiddingResultFactory.instance.createResult(Collections.emptyList());
	}

	@Test(expected = IllegalArgumentException.class)
	public void tooManyBids() {
		PairBiddingResultFactory.instance.createResult(Arrays.asList(Bid.ofBidder(new StaticBidder(1)),
				Bid.ofBidder(new StaticBidder(1)), Bid.ofBidder(new StaticBidder(1))));
	}

	@Test
	public void correctBiddersLeftWin() {
		final Result res = PairBiddingResultFactory.instance.createResult(Arrays.asList(higherBid, lowerBid));

		assertThat(res.getQuantity(lowerBidder), hasQuantity(0));
		assertThat(res.getQuantity(higherBidder), hasQuantity(2));
	}

	@Test
	public void correctBiddersRightWin() {
		final Result res = PairBiddingResultFactory.instance.createResult(Arrays.asList(lowerBid, higherBid));

		assertThat(res.getQuantity(lowerBidder), hasQuantity(0));
		assertThat(res.getQuantity(higherBidder), hasQuantity(2));
	}

	@Test
	public void correctBiddersTie() {
		final Result res = PairBiddingResultFactory.instance.createResult(Arrays.asList(equalBid, higherBid));

		assertThat(res.getQuantity(equalBidder), hasQuantity(1));
		assertThat(res.getQuantity(higherBidder), hasQuantity(1));
	}

	@Test
	public void bothZero() {
		assertThat(zeroBidA.getBidder(), not(equalTo(zeroBidB.getBidder())));
		final Result res = PairBiddingResultFactory.instance.createResult(Arrays.asList(zeroBidA, zeroBidB));

		assertThat(res.getQuantity(equalBidder), hasQuantity(0));
		assertThat(res.getQuantity(higherBidder), hasQuantity(0));
	}

	private static Matcher<Quantity> hasQuantity(int value) {
		return new ExtractingMatcher<Quantity, Integer>("quantity", Quantity::getQuantity, Matchers.equalTo(value));
	}

	private static final class StaticBidder implements IdentifiableBidder {
		private final int bidAmount;

		private StaticBidder(int bidAmount) {
			super();
			this.bidAmount = bidAmount;
		}

		@Override
		public void init(int quantity, int cash) {
			Failure.unexpectedCall();
		}

		@Override
		public int placeBid() {
			return bidAmount;
		}

		@Override
		public void bids(int own, int other) {
			Failure.unexpectedCall();
		}

		@Override
		public String getUuid() {
			return Failure.unexpectedCall();
		}

		@Override
		public String name() {
			return Failure.unexpectedCall();
		}

		@Override
		public Account getAccount() {
			return Failure.unexpectedCall();
		}
	}
}

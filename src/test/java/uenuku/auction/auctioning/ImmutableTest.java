package uenuku.auction.auctioning;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ImmutableTest extends uenuku.auction.ImmutableTestTemplate {

	public ImmutableTest(Class<?> cls) {
		super(cls);
	}

	@Parameters(name = "{index} {0}")
	public static Collection<Object[]> parameters() {
		return wrapList(Auction.class, Product.class, Bid.class, Offer.class, Quantity.class, Result.class);
	}
}

package uenuku.auction.auctioning;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import uenuku.auction.ExtractingMatcher;

public class ProductTest {
	@Test(expected = IllegalArgumentException.class)
	public void withdrawNegative() {
		new Product(7).withdraw(-4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void withdrawTooMuch() {
		final int initial = 7;
		new Product(initial).withdraw(initial + 1);
	}

	@Test
	public void withdrawCorrect() {
		final int initial = 7;
		final int withdraw = 4;
		final Product p = new Product(initial).withdraw(withdraw);
		assertThat(p, hasCurrentQuantity(initial - withdraw));
		assertThat(p, hasQuantity());
	}

	@Test
	public void withdrawAll() {
		final int initial = 7;
		final Product p = new Product(initial).withdraw(initial);
		assertThat(p, hasCurrentQuantity(0));
		assertThat(p, Matchers.not(hasQuantity()));
	}

	private static Matcher<Product> hasCurrentQuantity(int quantity) {
		return new ExtractingMatcher<Product, Integer>("currentQuantity", Product::getCurrentQuantity,
				Matchers.equalTo(quantity));
	}

	private static Matcher<Product> hasQuantity() {
		return new ExtractingMatcher<Product, Boolean>("hasQuantity", Product::hasQuantity, Matchers.equalTo(true));
	}
}

package uenuku.auction.auctioning;

import java.util.Arrays;
import java.util.Collections;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import uenuku.auction.account.Cashier;
import uenuku.auction.registration.Bidder;
import uenuku.auction.registration.StaticRegistration;
import uenuku.auction.state.TestBidder;

public class AuctionTest {

	@Before
	public void resetBidder() {
		Bidder.reset();
	}

	@Test(expected = IllegalArgumentException.class)
	public void toFewBidders() {
		new Auction(new Product(1), Collections.emptyList());
	}

	@Test(expected = IllegalArgumentException.class)
	public void toManyBidders() {
		new Auction(new Product(1), Arrays.asList(new TestBidder(), new TestBidder(), new TestBidder()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void unregisteredBidders() {
		new Auction(new Product(1), Arrays.asList(Bidder.Eyjafjallajökull, Bidder.Kohala));
	}

	@Test
	public void correctBidders() {
		final Product p = new Product(90);
		final Auction auction = new Auction(p, new StaticRegistration(new CashierImpl()).callForRegistration(p));
		Assert.assertThat(auction.offerAndSell(), Matchers.notNullValue());
	}

	private static class CashierImpl implements Cashier {

		@Override
		public Integer getBlueCash() {
			return 7;
		}

		@Override
		public Integer getRedCash() {
			return 13;
		}

	}

}

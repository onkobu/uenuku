package uenuku.auction;

import java.util.Arrays;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.rules.ExpectedException;

public final class Expectations {
	private final ExpectedException exceptionRule;

	private Expectations(ExpectedException exceptionRule) {
		this.exceptionRule = exceptionRule;
	}

	public static Expectations useRule(ExpectedException exceptionRule) {
		return new Expectations(exceptionRule);
	}

	public Expectations callThrowing(Class<? extends Throwable> throwable) {
		exceptionRule.expect(throwable);
		return this;
	}

	public Expectations inClass(Class<?> cls) {
		exceptionRule.expect(stackTraceContains(cls));
		return this;
	}

	private static Matcher<?> stackTraceContains(Class<?> cls) {
		return new TypeSafeDiagnosingMatcher<Throwable>() {

			@Override
			public void describeTo(Description description) {
				description.appendText(" stack trace containing ").appendValue(cls);
			}

			@Override
			protected boolean matchesSafely(Throwable item, Description mismatchDescription) {

				boolean found = false;
				for (StackTraceElement ele : item.getStackTrace()) {
					Class<?> source;
					try {
						source = Class.forName(ele.getClassName());
					} catch (ClassNotFoundException e) {
						throw new IllegalStateException(
								"A class threw an exception that can't be loaded: " + ele.getClassName(), e);
					}
					if (cls.equals(source)) {
						found = true;
						break;
					}
				}
				if (!found) {
					mismatchDescription.appendValueList(" class was not found in ", "\n", "",
							Arrays.asList(item.getStackTrace()));
				}
				return found;
			}
		};
	}
}

package uenuku.auction.registration;

import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import uenuku.auction.AccountMatchers;
import uenuku.auction.Expectations;
import uenuku.auction.account.Cashier;
import uenuku.auction.auctioning.Product;

public class StaticRegistrationTest {
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Before
	public void resetBidderStateMachine() {
		Bidder.reset();
	}

	@Test
	public void createNull() {
		// does not fail
		assertThat(new StaticRegistration(null).getCashier(), nullValue());
	}

	@Test
	public void createNullAndInit() {
		final Bidder bidder = Bidder.values()[0];
		// NPE could be caused by anything and @Test(expected) doesn't allow
		// thorough testing.
		Expectations.useRule(exceptionRule).callThrowing(NullPointerException.class).inClass(bidder.getClass());
		new StaticRegistration(null).callForRegistration(new Product(5));
	}

	@Test
	public void initWithCashier() {
		assertThat(Bidder.Eyjafjallajökull.getAccount(), Matchers.nullValue());
		final CashierImpl cashier = new CashierImpl();
		new StaticRegistration(cashier).callForRegistration(new Product(5));
		assertThat(Bidder.Eyjafjallajökull.getAccount(), AccountMatchers.hasBalance(cashier.getRedCash()));
		assertThat(Bidder.Kohala.getAccount(), AccountMatchers.hasBalance(cashier.getBlueCash()));
	}

	private static class CashierImpl implements Cashier {

		@Override
		public Integer getBlueCash() {
			return 7;
		}

		@Override
		public Integer getRedCash() {
			return 13;
		}

	}
}

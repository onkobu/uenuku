package uenuku.auction.state;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import mockit.Mocked;
import mockit.Verifications;
import uenuku.auction.FatalError;
import uenuku.auction.bidding.BidderStateMachine;
import uenuku.auction.bidding.BiddingStrategies;
import uenuku.auction.bidding.ErrorHandler;
import uenuku.auction.bidding.TransitionError;

/**
 * To wrap them in a class and give them context.
 *
 * @author onkobu
 *
 */
@RunWith(Enclosed.class)
public class BidderStateMachineTest {
	private static final Random RANDOM = new Random();

	/**
	 * Aspect of error handling.
	 *
	 * @author onkobu
	 *
	 */
	public static class FatalErrorHandlerTest {
		@Test
		public void callInitTwice(@Mocked ErrorHandler handler) {
			final BidderStateMachine bs = createStateMachine();
			bs.addErrorHandler(handler);
			bs.init(RANDOM.nextInt(), RANDOM.nextInt());
			bs.init(RANDOM.nextInt(), RANDOM.nextInt());

			new Verifications() {
				{
					handler.handleFatalError(FatalError.BiddedAlreadyInitiated);
					times = 1;
				}
			};
		}

		@Test
		public void addHandlerTwice(@Mocked ErrorHandler handler) {
			final BidderStateMachine bs = createStateMachine();
			bs.addErrorHandler(handler);
			bs.addErrorHandler(handler);
			bs.init(RANDOM.nextInt(), RANDOM.nextInt());
			bs.init(RANDOM.nextInt(), RANDOM.nextInt());

			new Verifications() {
				{
					handler.handleFatalError(FatalError.BiddedAlreadyInitiated);
					times = 2;
				}
			};
		}

		@Test
		public void addHandlerTwiceAndRemove(@Mocked ErrorHandler handler) {
			final BidderStateMachine bs = createStateMachine();
			bs.addErrorHandler(handler);
			bs.addErrorHandler(handler);
			bs.removeErrorHandler(handler);
			bs.init(RANDOM.nextInt(), RANDOM.nextInt());
			bs.init(RANDOM.nextInt(), RANDOM.nextInt());

			new Verifications() {
				{
					handler.handleFatalError(FatalError.BiddedAlreadyInitiated);
					times = 1;
				}
			};
		}

		private BidderStateMachine createStateMachine() {
			final BidderStateMachine bs = new BidderStateMachine(null, null);
			return bs;
		}
	}

	/**
	 * This test also illustrates the fallacy of a hand carved state machine: there
	 * may be untested/ unexpected transitions. A fully blown state machine with
	 * states and transitions can be tested through the full cross product of all
	 * states with all states held against the set of all transitions.
	 */
	public static class StateTransitionTest {
		@Test
		public void fromInitToPlace() {
			final RecordingErrorHandler errorHandler = new RecordingErrorHandler();
			final BidderStateMachine bs = createStateMachine(errorHandler);

			bs.init(Math.abs(RANDOM.nextInt()), Math.abs(RANDOM.nextInt()));
			bs.placeBid();
			assertThat(errorHandler, noErrors());
		}

		private BidderStateMachine createStateMachine(RecordingErrorHandler errorHandler) {
			final BidderStateMachine bs = new BidderStateMachine(new TestBidder(), BiddingStrategies.truelyRandomBid());
			bs.addErrorHandler(errorHandler);
			return bs;
		}
	}

	static Matcher<RecordingErrorHandler> noErrors() {
		return new TypeSafeDiagnosingMatcher<BidderStateMachineTest.RecordingErrorHandler>() {
			@Override
			public void describeTo(Description description) {
				description.appendText("no errors");
			}

			@Override
			protected boolean matchesSafely(RecordingErrorHandler item, Description mismatchDescription) {
				if (item.getErrorCount() == 0) {
					return true;
				}
				if (!item.getFatalErrors().isEmpty()) {
					mismatchDescription.appendValueList(" had fatal errors ", ",", "", item.getFatalErrors());
				}

				if (!item.getTransitionErrors().isEmpty()) {
					mismatchDescription.appendValueList(" had transition errors ", ",", "", item.getTransitionErrors());
				}
				return false;
			}
		};
	}

	static class RecordingErrorHandler implements ErrorHandler {
		private final List<FatalError> fatalErrors = new ArrayList<>();
		private final List<TransitionError> transitionErrors = new ArrayList<>();

		@Override
		public void handleFatalError(FatalError e, Object... args) {
			fatalErrors.add(e);
		}

		@Override
		public void handleTransitionError(TransitionError fe) {
			transitionErrors.add(fe);
		}

		public List<FatalError> getFatalErrors() {
			// copy-on-write-policy
			return new ArrayList<>(fatalErrors);
		}

		public List<TransitionError> getTransitionErrors() {
			// copy-on-write-policy
			return new ArrayList<>(transitionErrors);
		}

		public int getErrorCount() {
			return fatalErrors.size() + transitionErrors.size();
		}
	}
}

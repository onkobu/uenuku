package uenuku.auction;

/**
 * Programmatically fail tests.
 *
 * @author onkobu
 *
 */
public class Failure {
	public static <T> T unexpectedCall() {
		throw new AssertionError("unexpected call");
	}
}

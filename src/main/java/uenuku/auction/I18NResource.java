package uenuku.auction;

import java.util.Arrays;

public enum I18NResource {
	/**
	 * Tried to start auction with more or less than 2 bidders.
	 */
	EX_MUST_BE_BIDDER_PAIR,

	/**
	 * Attempt to withdraw more quantity than available.
	 */
	EX_PRODUCT_QUANTITY_TOO_LOW,

	/**
	 * Attemt to start an auction with unregistered bidders.
	 */
	EX_BIDDER_NOT_REGISTERED;

	public String getText() {
		// default until there's a ResourceBundle
		return name();
	}

	public String renderMessage(Object... args) {
		return String.format("%s, %s", getText(), Arrays.toString(args));
	}
}

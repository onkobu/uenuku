package uenuku.auction;

public enum FatalError {
	InitValidationFailed,

	BiddedAlreadyInitiated;
}

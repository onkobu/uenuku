package uenuku.auction;

import java.util.List;

import uenuku.auction.account.Cashier;
import uenuku.auction.auctioning.Product;

/**
 * All values after input validation.
 *
 * @author onkobu
 *
 */
public final class InputFixture implements Cashier {
	// These ought to be final, too but this would require a separate builder with a
	// termination factory-operation - effort needs to be limited somewhere.
	private Integer blueCash;
	private Integer redCash;
	private Product product;
	private List<String> errors;

	@Override
	public Integer getBlueCash() {
		return blueCash;
	}

	public InputFixture setBlueCash(Integer blueCash) {
		this.blueCash = blueCash;
		return this;
	}

	@Override
	public Integer getRedCash() {
		return redCash;
	}

	public InputFixture setRedCash(Integer redCash) {
		this.redCash = redCash;
		return this;
	}

	public Integer getQuantity() {
		return product.getInitialQuantity();
	}

	public Product getProduct() {
		return product;
	}

	public Cashier getCashier() {
		return this;
	}

	public InputFixture setQuantity(Integer quantity) {
		if (quantity != null) {
			this.product = new Product(quantity);
		}
		return this;
	}

	public List<String> getErrors() {
		return errors;
	}

	public InputFixture setErrors(List<String> errors) {
		this.errors = errors;
		return this;
	}

	public boolean hasErrors() {
		return errors != null && errors.size() > 0;
	}
}

package uenuku.auction.auctioning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Offer and outcome of a single auction step. This is not limited to only two
 * bidders and already supports many.
 *
 * @author onkobu
 *
 */
public class Offer {

	private final List<Bid> bids;

	private final ResultFactory resultFactory;

	/**
	 * Current default with a pair bidding strategy.
	 * 
	 * @param bids
	 */
	public Offer(List<Bid> bids) {
		this(PairBiddingResultFactory.instance, bids);
	}

	Offer(ResultFactory resultFactory, List<Bid> bids) {
		// copy to break reference and changing back through time and space
		this.bids = Collections.unmodifiableList(new ArrayList<>(bids));
		this.resultFactory = resultFactory;
	}

	public List<Bid> getBids() {
		return bids;
	}

	public Result getResult() {
		return resultFactory.createResult(bids);
	}

	@Override
	public String toString() {
		return "Offer: " + bids;
	}
}

package uenuku.auction.auctioning;

import java.util.List;

/**
 * Calculates the result, based on the list of bids of all bidders.
 *
 * @author onkobu
 *
 */
public interface ResultFactory {
	/**
	 * Create the result.
	 *
	 * @param bids List of all bids of all bidders.
	 * @return Result with quantities assigned to bidders according to factory's
	 *         rules.
	 */
	Result createResult(List<Bid> bids);

}

package uenuku.auction.auctioning;

import uenuku.auction.I18NResource;

/**
 * A product with fixed quantity.
 *
 * @author onkobu
 *
 */
public final class Product {
	private final int initialQuantity;
	private int currentQuantity;

	public Product(int quantity) {
		super();
		this.initialQuantity = quantity;
		this.currentQuantity = initialQuantity;
	}

	/**
	 *
	 * @return Quantity the product started with.
	 */
	public int getInitialQuantity() {
		return initialQuantity;
	}

	/**
	 *
	 * @return Current quantity.
	 */
	public int getCurrentQuantity() {
		return currentQuantity;
	}

	/**
	 * Reduce product's current quantity by the given amount.
	 *
	 * @throws IllegalArgumentException if more than current quantity is to be
	 *                                  withdrawn.
	 * @see #hasQuantity()
	 * @param offerAmount
	 */
	public Product withdraw(int offerAmount) {
		if (offerAmount > currentQuantity) {
			throw new IllegalArgumentException(
					I18NResource.EX_PRODUCT_QUANTITY_TOO_LOW.renderMessage(currentQuantity, offerAmount));
		}
		if (offerAmount < 0) {
			throw new IllegalArgumentException(
					I18NResource.EX_PRODUCT_QUANTITY_TOO_LOW.renderMessage(currentQuantity, offerAmount));
		}
		currentQuantity -= offerAmount;
		return this;
	}

	public boolean hasQuantity() {
		// must offer at least 2 whereas quantity could be odd
		return currentQuantity > 1;
	}
}

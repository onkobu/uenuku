package uenuku.auction.auctioning;

import java.util.Map;

import uenuku.auction.IdentifiableBidder;

/**
 * Entity transferring quantity result after bidding took place.
 *
 * @author onkobu
 *
 */
public final class Result {
	private final Map<IdentifiableBidder, Quantity> quantities;

	Result(Map<IdentifiableBidder, Quantity> quantities) {
		this.quantities = quantities;
	}

	/**
	 * Get the bidder's quantity.
	 *
	 * @param bidder
	 * @return Quantity won by the bidder. Even unregistered bidders get a quantity
	 *         of 0.
	 */
	public Quantity getQuantity(IdentifiableBidder bidder) {
		return quantities.getOrDefault(bidder, Quantity.empty());
	}

	@Override
	public String toString() {
		return quantities.entrySet().toString();
	}
}

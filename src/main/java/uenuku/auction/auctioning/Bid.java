package uenuku.auction.auctioning;

import uenuku.auction.IdentifiableBidder;

/**
 * An entity to introduce an auction-side (immutable) state. Tracks bidding
 * history.
 *
 * @author onkobu
 *
 */
public final class Bid {
	private final int moneyOffered;
	private final IdentifiableBidder bidder;

	private Bid(IdentifiableBidder bidder) {
		moneyOffered = bidder.placeBid();
		this.bidder = bidder;
	}

	public int getMoneyOffered() {
		return moneyOffered;
	}

	public IdentifiableBidder getBidder() {
		return bidder;
	}

	public static Bid ofBidder(IdentifiableBidder bidder) {
		return new Bid(bidder);
	}

	@Override
	public String toString() {
		return String.format("%s offered %d", bidder.name(), moneyOffered);
	}
}
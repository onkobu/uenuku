package uenuku.auction.auctioning;

/**
 * An immutable quantity during bidding.
 *
 * @author onkobu
 *
 */
public final class Quantity {
	private static final Quantity EMPTY = new Quantity(0);

	private final int quantity;

	public Quantity(int quantity) {
		super();
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	public static Quantity empty() {
		return EMPTY;
	}

	@Override
	public String toString() {
		return "+" + String.valueOf(quantity);
	}
}

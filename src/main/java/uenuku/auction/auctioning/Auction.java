package uenuku.auction.auctioning;

import static uenuku.auction.Constants.OFFER_AMOUNT;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import uenuku.auction.I18NResource;
import uenuku.auction.IdentifiableBidder;

/**
 * State of an auction.
 *
 * @author onkobu
 *
 */
public class Auction {
	private final Product product;
	private final List<IdentifiableBidder> bidders;

	public Auction(Product product, List<IdentifiableBidder> bidders) {
		super();
		if (bidders.size() != 2) {
			throw new IllegalArgumentException(I18NResource.EX_MUST_BE_BIDDER_PAIR.renderMessage(bidders.size()));
		}
		List<IdentifiableBidder> unregistered = bidders.stream().filter(b -> b.getAccount() == null)
				.collect(Collectors.toList());
		if (!unregistered.isEmpty()) {
			throw new IllegalArgumentException(I18NResource.EX_BIDDER_NOT_REGISTERED.renderMessage(
					unregistered.stream().map(IdentifiableBidder::name).collect(Collectors.joining(", "))));
		}
		this.product = product;
		this.bidders = bidders;
	}

	public boolean hasQuantity() {
		return product.hasQuantity();
	}

	public Offer offerAndSell() {
		if (bidders.size() != 2) {
			throw new IllegalStateException(I18NResource.EX_MUST_BE_BIDDER_PAIR.renderMessage(bidders.size()));
		}
		// If there were >100 bidders a HashMap becomes more efficient until then
		// walking the array is cheapest
		List<Bid> bids = new ArrayList<Bid>();
		for (IdentifiableBidder bidder : bidders) {
			bids.add(Bid.ofBidder(bidder));
		}
		// From here contract binds hard, only two bidders due to method signature for
		// bid exchange. This must change for future versions supporting multiple
		// bidders.
		final IdentifiableBidder bidderRed = bidders.get(0);
		final Bid bidRed = bids.get(0);
		final IdentifiableBidder bidderBlue = bidders.get(1);
		final Bid bidBlue = bids.get(1);
		bidderRed.bids(bidRed.getMoneyOffered(), bidBlue.getMoneyOffered());
		bidderBlue.bids(bidBlue.getMoneyOffered(), bidRed.getMoneyOffered());
		product.withdraw(OFFER_AMOUNT);
		return new Offer(bids);
	}
}

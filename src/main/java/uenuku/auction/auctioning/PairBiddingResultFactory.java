package uenuku.auction.auctioning;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uenuku.auction.Constants;
import uenuku.auction.I18NResource;
import uenuku.auction.IdentifiableBidder;

public final class PairBiddingResultFactory implements ResultFactory {

	// ClassLoader tightens our singleton together
	final static ResultFactory instance = new PairBiddingResultFactory();

	private PairBiddingResultFactory() {

	}

	@Override
	public Result createResult(List<Bid> bids) {
		if (bids.size() != 2) {
			throw new IllegalArgumentException(I18NResource.EX_MUST_BE_BIDDER_PAIR.renderMessage(bids.size()));
		}
		final Bid first = bids.get(0);
		final Bid second = bids.get(1);
		final Map<IdentifiableBidder, Quantity> quantities = new HashMap<>();
		if (first.getMoneyOffered() > 0 && first.getMoneyOffered() > second.getMoneyOffered()) {
			quantities.put(first.getBidder(), new Quantity(Constants.OFFER_AMOUNT));
		} else if (first.getMoneyOffered() == second.getMoneyOffered() && first.getMoneyOffered() > 0) {
			quantities.put(first.getBidder(), new Quantity(Constants.OFFER_AMOUNT >> 1));
			quantities.put(second.getBidder(), new Quantity(Constants.OFFER_AMOUNT >> 1));
		} else if (second.getMoneyOffered() > 0 && second.getMoneyOffered() > first.getMoneyOffered()) {
			quantities.put(second.getBidder(), new Quantity(Constants.OFFER_AMOUNT));
		} // all other cases nobody gets anything
		return new Result(quantities);
	}

}

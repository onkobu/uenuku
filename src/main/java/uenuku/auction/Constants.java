package uenuku.auction;

/**
 * Collection of immutable values, magic numbers and configuration.
 * 
 * @author onkobu
 *
 */
public interface Constants {
	/**
	 * Quantity for each offer subtracted from product's total quantity.
	 */
	int OFFER_AMOUNT = 2;
}

package uenuku.auction.registration;

import java.util.UUID;

import uenuku.auction.account.Account;
import uenuku.auction.account.Cashier;
import uenuku.auction.auctioning.Product;
import uenuku.auction.bidding.BidderStateMachine;
import uenuku.auction.bidding.BiddingFigure;
import uenuku.auction.bidding.BiddingStrategies;
import uenuku.auction.bidding.BiddingStrategy;
import uenuku.auction.bidding.UnforgivingErrorHandler;

/**
 * To give bidders name as well as shape and limit the options. Volcanos are
 * sufficiently mythic and real to act as bidders.
 *
 * @author onkobu
 *
 */
public enum Bidder implements uenuku.auction.IdentifiableBidder {
	/**
	 * Volcano on Hawaii, Big Island.
	 */
	Kohala(BiddingStrategies.truelyRandomBid()) {
		@Override
		public void init(Product product, Cashier cashier) {
			init(product.getInitialQuantity(), cashier.getBlueCash());
		}
	},

	/**
	 * Volcano on Island
	 */
	Eyjafjallajökull(BiddingStrategies.truelyRandomBid()) {
		@Override
		public void init(Product product, Cashier cashier) {
			init(product.getInitialQuantity(), cashier.getRedCash());
		}
	};

	private final BidderStateMachine stateMachine;
	private final BiddingStrategy biddingStrategy;
	private final String uuid = UUID.randomUUID().toString();

	private Bidder(BiddingStrategy strategy) {
		this.biddingStrategy = strategy;
		// Let's believe in the good state machine not messing with the incomplete
		// this-reference. Luckily it is only IdentifiableBidder, not a true this.
		stateMachine = new BidderStateMachine(this, biddingStrategy);
		stateMachine.addErrorHandler(new UnforgivingErrorHandler());
	}

	@Override
	public String getUuid() {
		return uuid;
	}

	@Override
	public void init(int quantity, int cash) {
		stateMachine.init(quantity, cash);
	}

	@Override
	public int placeBid() {
		return stateMachine.placeBid();
	}

	@Override
	public void bids(int own, int other) {
		stateMachine.bids(own, other);
	}

	/**
	 * Repeats the init-method in a type safe way and delegates.
	 *
	 * @param product Product to bid on.
	 * @param cashier Money provider.
	 */
	public abstract void init(Product product, Cashier cashier);

	public BiddingFigure createFigure() {
		return stateMachine.createFigure();
	}

	@Override
	public Account getAccount() {
		return stateMachine.getAccount();
	}

	private void resetState() {
		stateMachine.reset();
	}

	public static void reset() {
		for (Bidder bidder : values()) {
			bidder.resetState();
		}
	}
}

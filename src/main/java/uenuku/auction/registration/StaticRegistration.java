package uenuku.auction.registration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uenuku.auction.IdentifiableBidder;
import uenuku.auction.account.Cashier;
import uenuku.auction.auctioning.Product;

/**
 * Static implementation of a registration. Enum {@link Bidder} is used to full
 * extend.
 *
 * @author onkobu
 *
 */
public class StaticRegistration implements Registration {
	private final Cashier cashier;

	/**
	 *
	 * @param cashier Inject the source of money.
	 */
	public StaticRegistration(Cashier cashier) {
		super();
		this.cashier = cashier;
	}

	public Cashier getCashier() {
		return cashier;
	}

	@Override
	public List<IdentifiableBidder> callForRegistration(Product product) {
		List<IdentifiableBidder> bidders = new ArrayList<>();
		for (Bidder b : Bidder.values()) {
			b.init(product, cashier);
			bidders.add(b);
		}
		return Collections.unmodifiableList(bidders);
	}

}

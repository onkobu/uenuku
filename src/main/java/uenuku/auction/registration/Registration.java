package uenuku.auction.registration;

import java.util.List;

import uenuku.auction.IdentifiableBidder;
import uenuku.auction.auctioning.Product;

/**
 * Notifying bidders about a starting auction.
 *
 * @author onkobu
 *
 */
public interface Registration {
	/**
	 * Contact all available bidders and offer the product.
	 * 
	 * @param product Product the bidders will bid for.
	 * @return List of bidders taking part in auction.
	 */
	List<IdentifiableBidder> callForRegistration(Product product);
}

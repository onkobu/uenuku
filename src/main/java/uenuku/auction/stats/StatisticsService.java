package uenuku.auction.stats;

import java.util.List;

import uenuku.auction.auctioning.Offer;

public interface StatisticsService {
	void renderStatistics(List<Offer> offers);
}

package uenuku.auction.stats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import uenuku.auction.auctioning.Offer;
import uenuku.auction.bidding.BiddingFigure;
import uenuku.auction.registration.Bidder;

public class SystemOutStatsService implements StatisticsService {
	@Override
	public void renderStatistics(List<Offer> offers) {
		offers.stream().forEach((offer) -> System.out.printf("%s -> %s%n", offer, offer.getResult()));

		// Despite the pair-nature of the entire auction this is already prepared for a
		// much larger number of bidders.
		List<BiddingFigure> figuresByQuantity = new ArrayList<>();
		List<BiddingFigure> figuresByLowestPrice = new ArrayList<>();
		for (Bidder bidder : Bidder.values()) {
			final BiddingFigure fig = bidder.createFigure();
			figuresByQuantity.add(fig);
			figuresByLowestPrice.add(fig);
			System.out.println(fig);
		}
		Collections.sort(figuresByQuantity, BiddingFigure.QUANTITY_COMPARATOR);
		Collections.sort(figuresByLowestPrice, BiddingFigure.LOWEST_PRICE_COMPARATOR);
		System.out.println("Ranking highest quantity to lowest: "
				+ figuresByQuantity.stream().map((f) -> f.getBidder().name()).collect(Collectors.joining(", ")));
		System.out.println("Ranking lowest average price to highest: "
				+ figuresByLowestPrice.stream().map((f) -> f.getBidder().name()).collect(Collectors.joining(", ")));

	}
}

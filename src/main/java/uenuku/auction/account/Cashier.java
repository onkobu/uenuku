package uenuku.auction.account;

/**
 * A vehicle to put money into the bidders. Normally this is hidded but due to
 * the nature of this runtime decoupling is necessary.
 *
 * @author onkobu
 *
 */
public interface Cashier {
	/**
	 *
	 * @return Amount for blue side.
	 */
	Integer getBlueCash();

	/**
	 *
	 * @return Amount for red side.
	 */
	Integer getRedCash();
}

package uenuku.auction.account;

/**
 * Tracks balance/ account during bidding.
 *
 * @author onkobu
 *
 */
public class Account {
	private final int initialAmount;

	private int currentAmount;

	public Account(int cash) {
		this.initialAmount = cash;
		this.currentAmount = cash;
	}

	/**
	 * Try to take the given amount of cash from the account. This can be 0 due to
	 * empty account, less than offer because of insufficient funds or the full
	 * offer.
	 *
	 * @param offer Amount of cash to be taken
	 * @return True amount withdrawn.
	 */
	public synchronized int withdraw(int offer) {
		// Since there is no other object taken into consideration locking cannot be
		// made much smaller. Checking and subtracting must be atomic. AtomicInteger
		// is not much more atomic and would need several attempts around compareAndSet
		if (currentAmount == 0) {
			return 0;
		}

		if (currentAmount < offer) {
			final int remainingAmount = currentAmount;
			currentAmount = 0;
			return remainingAmount;
		}
		currentAmount -= offer;
		return offer;
	}

	public int getBalance() {
		return currentAmount;
	}

	public boolean isEmpty() {
		return currentAmount == 0;
	}

	public int getInitialAmount() {
		return initialAmount;
	}
}

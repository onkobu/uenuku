package uenuku.auction;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Supplier;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import uenuku.auction.auctioning.Offer;
import uenuku.auction.auctioning.Product;
import uenuku.auction.bidding.BiddingService;
import uenuku.auction.bidding.ProductCountdownBidding;
import uenuku.auction.registration.Registration;
import uenuku.auction.registration.StaticRegistration;
import uenuku.auction.stats.StatisticsService;
import uenuku.auction.stats.SystemOutStatsService;

/**
 * Main class, entry point and runner of the entire auction platform.
 *
 * @author onkobu
 *
 */
public final class AuctionRunner {
	private static final String OPTION_HELP = "h";

	private static final String OPTION_QUANTITY = "q";

	private static final String OPTION_RED = "r";

	private static final String OPTION_BLUE = "b";

	private static final Options options = new Options()//
			.addOption(Option.builder(OPTION_HELP).longOpt("help").desc("Usage").build())
			.addOption(Option.builder(OPTION_QUANTITY).required().longOpt("quantity").desc("quantity of product")
					.hasArg().build())//
			.addOption(Option.builder(OPTION_RED).required().longOpt("red").desc("Red's cash").hasArg().build())//
			.addOption(Option.builder(OPTION_BLUE).required().longOpt("blue").desc("Blue's cash").hasArg().build());

	private final Product product;

	private final Supplier<Registration> registrationSupplier;

	private final Supplier<BiddingService> biddingServiceSupplier;

	private final StatisticsService statisticsService = new SystemOutStatsService();

	private AuctionRunner(Product p, Supplier<Registration> registrationSupplier,
			Supplier<BiddingService> biddingServiceSupplier) {
		this.product = p;
		this.registrationSupplier = registrationSupplier;
		this.biddingServiceSupplier = biddingServiceSupplier;
	}

	public enum ValidationError {
		INVALID_NUMBER;

		public String getText() {
			return ResourceBundle.getBundle("uenuku.ValidationError").getString(name());
		}

		public String renderMessage(Object... args) {
			return String.format(getText(), args);
		}
	}

	public static void main(String[] args) throws Exception {
		final InputFixture inputFixture = validateInput(args);

		// printed usage already
		if (inputFixture == null) {
			return;
		}

		if (inputFixture.hasErrors()) {
			inputFixture.getErrors().stream().forEach(System.err::println);
			return;
		}
		new AuctionRunner(inputFixture.getProduct(), () -> new StaticRegistration(inputFixture.getCashier()),
				() -> new ProductCountdownBidding()).run();
	}

	static InputFixture validateInput(String[] args) throws ParseException {
		final CommandLineParser parser = new DefaultParser();
		final CommandLine cmd;
		try {
			cmd = parser.parse(options, args);
		} catch (MissingOptionException ex) {
			printUsage();
			return null;
		}
		if (args.length == 0 || cmd.hasOption(OPTION_HELP)) {
			printUsage();
			return null;
		}
		final String quantityText = cmd.getOptionValue(OPTION_QUANTITY);
		final String blueCashText = cmd.getOptionValue(OPTION_BLUE);
		final String redCashText = cmd.getOptionValue(OPTION_RED);

		final List<String> errors = new ArrayList<>();
		final Integer quantity = toIntegerValue(quantityText);
		if (quantity == null) {
			errors.add(ValidationError.INVALID_NUMBER.renderMessage("quantity", quantityText));
		}
		final Integer blueCash = toIntegerValue(blueCashText);
		if (blueCash == null) {
			errors.add(ValidationError.INVALID_NUMBER.renderMessage("blue cash", quantityText));
		}
		final Integer redCash = toIntegerValue(redCashText);
		if (redCash == null) {
			errors.add(ValidationError.INVALID_NUMBER.renderMessage("red cash", quantityText));
		}

		return new InputFixture().setQuantity(quantity).setBlueCash(blueCash).setRedCash(redCash).setErrors(errors);
	}

	private void run() {
		final List<Offer> offers = bidding();
		validate(offers);
		renderStatistics(offers);
		destroy();
	}

	private List<Offer> bidding() {
		return biddingServiceSupplier.get().bidding(product, registrationSupplier.get().callForRegistration(product));
	}

	private void validate(List<Offer> offers) {
		// Call a regulationService to check all offers for compliance
		// or at least sum up all offers' quantity and money amount to match balances
	}

	private void renderStatistics(List<Offer> offers) {
		statisticsService.renderStatistics(offers);
	}

	private void destroy() {
		// Housework before shutting down

	}

	static Integer toIntegerValue(String quantityText) {
		try {
			return Integer.parseInt(quantityText);
		} catch (NumberFormatException ex) {
			return null;
		}
	}

	private static void printUsage() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("auction [options] content",
				"Simulate an auction for a product with given quantity and two bidders red and blue with limited cash for each. Winner is the bidder with most product quantity or least spendings",
				options, "All values must be positive integers, 0 is allowed.", false);
	}
}

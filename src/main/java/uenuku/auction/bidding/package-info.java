/**
 * The bidder's side. This could be realized as a standalone service. Choose
 * your preferred style of coupling. I recommend events and a message bus.
 */
package uenuku.auction.bidding;

package uenuku.auction.bidding;

import java.util.UUID;

/**
 * An immutable entry for the bid history.
 *
 * @author onkobu
 *
 */
public class BidHistory {
	private final int cashAmount;
	private final int othersCashAmount;
	private final UUID uuid = UUID.randomUUID();
	private final UUID bidUuid;

	public BidHistory(Bid bid, int other) {
		this.cashAmount = bid.getCashAmount();
		this.bidUuid = bid.getUuid();
		this.othersCashAmount = other;
	}

	public int getCashAmount() {
		return cashAmount;
	}

	public int getOthersCashAmount() {
		return othersCashAmount;
	}

	public UUID getUuid() {
		return uuid;
	}

	public UUID getBidUuid() {
		return bidUuid;
	}

	public static BidHistory of(Bid bid, int other) {
		return new BidHistory(bid, other);
	}

	public int getAchievedQuantity() {
		// The specification does not state this explicitely, other's amount could also
		// be 0 and equality means 1:1. But no cash often yields no goods/ service/
		// value.
		if (cashAmount == 0) {
			return 0;
		}
		if (cashAmount > othersCashAmount) {
			return 2;
		}
		if (cashAmount == othersCashAmount) {
			return 1;
		}
		return 0;
	}

}

package uenuku.auction.bidding;

import uenuku.auction.Bidder;

/**
 * Adding callbacks to the bare bidding contract. Only <code>void</code>-return
 * type leaves not much options for meaningful responses or error handling.
 *
 * @author onkobu
 *
 */
public interface ErrorSource extends Bidder {

	/**
	 * Add an error handler callback. If added multiple times it'll be called
	 * multiple times.
	 *
	 * @param eh
	 */
	boolean addErrorHandler(ErrorHandler eh);

	/**
	 * Remove an error handler callback. Removes the first occurrence if added
	 * multiple times.
	 *
	 * @param eh
	 * @return
	 */
	boolean removeErrorHandler(ErrorHandler eh);

}

package uenuku.auction.bidding;

/**
 * A severe programming error. Input was validated upon entry into the system.
 * 
 * @author onkobu
 *
 */
public class BidNegativeAmountException extends RuntimeException {

}

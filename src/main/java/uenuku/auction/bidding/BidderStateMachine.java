package uenuku.auction.bidding;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import uenuku.auction.Bidder;
import uenuku.auction.FatalError;
import uenuku.auction.IdentifiableBidder;
import uenuku.auction.account.Account;

/**
 * Realizes contract for automatic bidding, tracking (account) balance and
 * quantity.
 *
 * @author onkobu
 *
 */
public class BidderStateMachine implements Bidder, ErrorSource {
	private final CopyOnWriteArrayList<ErrorHandler> errorHandlers = new CopyOnWriteArrayList<>();

	private Account account;

	private Quantity quantity;

	private Bid currentBid;

	private final BiddingStrategy biddingStrategy;

	private final List<BidHistory> bidHistory = new ArrayList<>();

	private final IdentifiableBidder bidder;

	public BidderStateMachine(IdentifiableBidder bidder, BiddingStrategy biddingStrategy) {
		super();
		this.biddingStrategy = biddingStrategy;
		this.bidder = bidder;
	}

	@Override
	public void init(int quantity, int cash) {
		if (quantity < 0 || cash < 0) {
			handle(FatalError.InitValidationFailed, quantity, cash);
		}
		if (account != null) {
			handle(FatalError.BiddedAlreadyInitiated);
			return;
		}
		account = new Account(cash);
		this.quantity = new Quantity();
	}

	@Override
	public int placeBid() {
		if (currentBid != null) {
			handle(TransitionError.BidInProgress);
			// TODO: magic number, mark in protocol
			return -1;
		}
		if (account == null) {
			handle(TransitionError.InitNotCalled);
		}
		currentBid = biddingStrategy.placeBid(account);
		return currentBid.getCashAmount();
	}

	@Override
	public void bids(int own, int other) {
		if (currentBid == null) {
			handle(TransitionError.NoBidInProgress);
		}
		if (currentBid.getCashAmount() != own) {
			handle(TransitionError.NotOwnBid);
			return;
		}

		final BidHistory bidHist = currentBid.finishBid(other);
		bidHistory.add(bidHist);
		quantity.deposit(bidHist.getAchievedQuantity());
		currentBid = null;
	}

	@Override
	public boolean addErrorHandler(ErrorHandler eh) {
		return errorHandlers.add(eh);
	}

	@Override
	public boolean removeErrorHandler(ErrorHandler eh) {
		return errorHandlers.remove(eh);
	}

	private void handle(FatalError fe, Object... args) {
		for (final ErrorHandler eh : errorHandlers) {
			eh.handleFatalError(fe, args);
		}
	}

	private void handle(TransitionError fe) {
		for (final ErrorHandler eh : errorHandlers) {
			eh.handleTransitionError(fe);
		}
	}

	public BiddingFigure createFigure() {
		return new BiddingFigure(bidder, account, quantity);
	}

	public Account getAccount() {
		return account;
	}

	public void reset() {
		account = null;
		quantity = null;
		bidHistory.clear();
		currentBid = null;
	}
}

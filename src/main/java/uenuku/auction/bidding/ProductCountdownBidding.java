package uenuku.auction.bidding;

import java.util.ArrayList;
import java.util.List;

import uenuku.auction.IdentifiableBidder;
import uenuku.auction.auctioning.Auction;
import uenuku.auction.auctioning.Offer;
import uenuku.auction.auctioning.Product;

public class ProductCountdownBidding implements BiddingService {

	@Override
	public List<Offer> bidding(Product product, List<IdentifiableBidder> bidder) {
		final Auction auction = new Auction(product, bidder);
		final List<Offer> offers = new ArrayList<Offer>();
		while (auction.hasQuantity()) {
			offers.add(auction.offerAndSell());
		}
		return offers;
	}

}

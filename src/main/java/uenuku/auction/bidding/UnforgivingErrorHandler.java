package uenuku.auction.bidding;

import java.util.Arrays;

import uenuku.auction.FatalError;

public class UnforgivingErrorHandler implements ErrorHandler {

	@Override
	public void handleFatalError(FatalError e, Object... args) {
		throw new IllegalStateException(String.format("%s (%s)", e, Arrays.toString(args)));
	}

	@Override
	public void handleTransitionError(TransitionError fe) {
		System.err.printf("Transition failure %s%n", fe);
	}

}

package uenuku.auction.bidding;

import uenuku.auction.FatalError;

/**
 * Handles errors during state transitions when bidding and accouting.
 * ErrorHandlers are managed with a copy-on-write-strategy. If an ErrorHandler
 * is added (even from another thread) while error handling is in progress it
 * won't be notified. After registration is finished it is going to receive all
 * error handling calls.
 * <p>
 * De-registration works exactly the same, with copy-on-write. Until the handler
 * is fully removed it'll receive error handling calls.
 *
 * @author onkobu
 *
 */
public interface ErrorHandler {
	/**
	 * Called when the state transition is absolutely invalid and no further
	 * progress is possible. Bidding must be terminated.
	 *
	 * @param e
	 * @param args
	 */
	void handleFatalError(FatalError e, Object... args);

	/**
	 * An error in state transition. States are resilient with clear transition
	 * rules. If such errors occur any modification was blocked and the state
	 * machine is still in sane condition.
	 *
	 * @param fe
	 */
	void handleTransitionError(TransitionError fe);
}

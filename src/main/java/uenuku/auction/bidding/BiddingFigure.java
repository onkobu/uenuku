package uenuku.auction.bidding;

import java.util.Comparator;

import uenuku.auction.IdentifiableBidder;
import uenuku.auction.account.Account;

/**
 * The math after auction is done.
 *
 * @author onkobu
 *
 */
public final class BiddingFigure {

	/**
	 * Highest quantity first. If same order is random.
	 */
	public static final Comparator<BiddingFigure> QUANTITY_COMPARATOR = (left, right) -> right.getQuantityGranted()
			- left.getQuantityGranted();
	/**
	 * Lowest average price first.
	 */
	public static final Comparator<BiddingFigure> LOWEST_PRICE_COMPARATOR = (left, right) -> {
		if (left.getAveragePrice() > right.getAveragePrice()) {
			return -1;
		}
		if (left.getAveragePrice() < right.getAveragePrice()) {
			return 1;
		}
		return 0;
	};

	private final int initialCash;
	private final int finalCash;
	private final int quantityGranted;
	private final IdentifiableBidder bidder;
	private final double averagePrice;

	public BiddingFigure(IdentifiableBidder bidder, Account account, Quantity quantity) {
		initialCash = account.getInitialAmount();
		finalCash = account.getBalance();
		quantityGranted = quantity.getCurrentQuantity();
		averagePrice = (double) (initialCash - finalCash) / quantityGranted;
		this.bidder = bidder;
	}

	public int getInitialCash() {
		return initialCash;
	}

	public int getFinalCash() {
		return finalCash;
	}

	public int getQuantityGranted() {
		return quantityGranted;
	}

	/**
	 * Due to the lack of units for the base values double is sufficient for this
	 * prototype. When using productively this must be projected onto integer
	 * numbers by scaling and rounding – subject to customer's requirements.
	 *
	 * @return Average price. Be aware of the limitations of double regarding
	 *         precision.
	 */
	public double getAveragePrice() {
		return averagePrice;
	}

	public IdentifiableBidder getBidder() {
		return bidder;
	}

	@Override
	public String toString() {
		return String.format("%s: %d money initial, %d money left, %d quantity granted, %.2f money/quantity",
				bidder.name(), initialCash, finalCash, quantityGranted, averagePrice);
	}
}

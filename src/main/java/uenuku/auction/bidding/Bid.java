package uenuku.auction.bidding;

import java.util.UUID;

/**
 * An immutable bid.
 *
 * @author onkobu
 *
 */
public final class Bid {
	private static final Bid EMPTY_BID = new Bid(0);

	private final int cashAmount;

	private final UUID uuid = UUID.randomUUID();

	public Bid(int cashAmount) {
		super();
		if (cashAmount < 0) {
			throw new BidNegativeAmountException();
		}
		this.cashAmount = cashAmount;
	}

	public int getCashAmount() {
		return cashAmount;
	}

	public UUID getUuid() {
		return uuid;
	}

	public BidHistory finishBid(int other) {
		return BidHistory.of(this, other);
	}

	public static Bid empty() {
		return EMPTY_BID;
	}

	public static Bid of(int amount) {
		return new Bid(amount);
	}

}

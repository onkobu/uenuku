package uenuku.auction.bidding;

/**
 * Tracks quantity during bidding.
 *
 * @author onkobu
 *
 */
public class Quantity {
	private int quantity;

	public void deposit(int achievedQuantity) {
		quantity += achievedQuantity;
	}

	public int getCurrentQuantity() {
		return quantity;
	}
}

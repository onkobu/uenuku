package uenuku.auction.bidding;

public enum TransitionError {
	/**
	 * Attempt to place a new bid while bidding is still in progress. Bidding
	 * continues, new bid is rejected.
	 */
	BidInProgress,

	/**
	 * Attemt to finish bidding with a different own cash amount than originally
	 * placed.
	 */
	NotOwnBid,

	/**
	 * Attempt to finish bidding without a placed bid.
	 */
	NoBidInProgress,

	/**
	 * Bidding started without calling init beforehand.
	 */
	InitNotCalled;

}

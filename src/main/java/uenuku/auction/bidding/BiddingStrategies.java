package uenuku.auction.bidding;

import java.util.Random;

/**
 * Collection of bidding strategies.
 *
 * @author onkobu
 *
 */
public final class BiddingStrategies {
	// Introduces a synchronization point thus limiting throughput
	private static final Random RANDOM = new Random();

	private BiddingStrategies() {
	}

	/**
	 * Takes the bidders account balance as upper limit (exclusive) and suggests a
	 * random value between 0 and upper limit.
	 * 
	 * @return
	 */
	public static BiddingStrategy truelyRandomBid() {
		// anonymous class or functional interface turns out the same
		return (acc) -> {
			return Bid.of(acc.withdraw(RANDOM.nextInt(acc.getBalance())));
		};
	}
}

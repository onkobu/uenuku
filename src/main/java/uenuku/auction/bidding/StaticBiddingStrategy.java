package uenuku.auction.bidding;

import uenuku.auction.account.Account;

public final class StaticBiddingStrategy implements BiddingStrategy {
	private final int bidAmount;

	private StaticBiddingStrategy(int bidAmount) {
		super();
		this.bidAmount = bidAmount;
	}

	@Override
	public Bid placeBid(Account account) {
		if (account.isEmpty()) {
			return Bid.empty();
		}

		return Bid.of(account.withdraw(bidAmount));
	}

	public static BiddingStrategy eachBid(int i) {
		return new StaticBiddingStrategy(i);
	}

}

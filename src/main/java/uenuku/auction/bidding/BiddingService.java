package uenuku.auction.bidding;

import java.util.List;

import uenuku.auction.IdentifiableBidder;
import uenuku.auction.auctioning.Offer;
import uenuku.auction.auctioning.Product;

public interface BiddingService {
	List<Offer> bidding(Product product, List<IdentifiableBidder> bidder);
}

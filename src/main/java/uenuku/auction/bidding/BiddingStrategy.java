package uenuku.auction.bidding;

import uenuku.auction.account.Account;

/**
 * Contract to create bids based on an algorithm, that can only access the
 * current account and it's own inner state.
 * 
 * @author onkobu
 *
 */
public interface BiddingStrategy {

	/**
	 * Create a bid based on the account balance and according to the strategy's
	 * algorithm.
	 *
	 * @param account Account to withdraw from.
	 * @return The bid holding the cash withdrawn from account.
	 */
	Bid placeBid(Account account);

}

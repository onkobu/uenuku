package uenuku.auction;

import uenuku.auction.account.Account;

public interface IdentifiableBidder extends Bidder {
	/**
	 * A world wide unique identifier for the records and tracking of bidders.
	 *
	 * @return UUID.
	 */
	String getUuid();

	/**
	 * Something humand readable, a name to relate to personally.
	 *
	 * @return Name, not unique.
	 */
	String name();

	/**
	 *
	 * @return Current account.
	 */
	Account getAccount();

}
